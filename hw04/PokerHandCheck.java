///////////////////////////////////
///// CSE2 PokerHandCheck 
/// Rebecca Price 2/15/19
/// purpose of the program is to generate random cards from 5 different decks of cards and then state

public class PokerHandCheck{
  public static void main (String args[]){
    
    
    //creating several integers that a re randomly generated
    //these will be converted into types of cards
    int card1 = (int) (Math.random()*52 + 1);
    
    int card2 = (int) (Math.random()*52 + 1);
    
    int card3 = (int) (Math.random()*52 + 1);
    
    int card4 = (int) (Math.random()*52 + 1);
    
    int card5 = (int) (Math.random()*52 +1 );
  
    //initialzing each of these cards so they can be later stored with the number of card
    String cardNumber1 = "1";
    
      String cardType1 = "1";
    
    String cardNumber2 = "2";
    
      String cardType2 = "2";
    
    String cardNumber3 = "3";
    
      String cardType3 = "3";
    
    String cardNumber4 = "4";
    
      String cardType4 = "4";
    
    String cardNumber5 = "5";
    
      String cardType5 = "5";
    
    
    //defines the suit of cardType1
    if (card1 <= 13){
        
          cardType1 = "Diamonds"; // states that if the number between 1 and 13 the number is a diamond
      }
       
      else if ( 13 < card1 && card1 <= 26 ){
          
          cardType1 = "Clubs";// states that if the number is between 14 and 26 the number is a club
      }
      
      else if ( 26 < card1 && card1 <= 39){
              
          cardType1 = "Hearts";//states that if the number is between 27 and 39 the number is a heart
       }
      
      else if (card1 > 39 && card1 <= 52){
        
          cardType1 = "Spades";//states that if the number is between 40 and 52 the number is a spade
      }
     
    
    //storing number for first card
    switch(card1 % 13 ) {
            case 0 :
             cardNumber1 = "King";
                break;
            case 1 :
             cardNumber1 = "Ace";
                break;
            case 2 :
            cardNumber1 = "2";
                break;
            case 3 :
            cardNumber1 = "3";
                break;
            case 4 :
            cardNumber1 = "4";
                break;
            case 5 :
            cardNumber1 = "5";
                break;
            case 6 :
            cardNumber1 = "6";
                break;
            case 7 :
            cardNumber1 = "7";
                break;
            case 8 :
            cardNumber1 = "8";
                break;
            case 9 :
             cardNumber1 = "9";
                break;
            case 10 :
             cardNumber1 = "10";
                break;
            case 11 :
             cardNumber1 = "Jack";
                break;
            case 12 :
             cardNumber1 = "Queen";
                break;
            default :
              System.out.print("");
                break;
    }
    
    
    //defines the suit of cardType2
    if (card2 <= 13){
        
          cardType2 = "Diamonds"; // states that if the number between 1 and 13 the number is a diamond
      }
       
      else if ( 13 < card2 && card2 <= 26 ){
          
          cardType2 = "Clubs";// states that if the number is between 14 and 26 the number is a club
      }
      
      else if ( 26 < card2 && card2 <= 39){
              
          cardType2 = "Hearts";//states that if the number is between 27 and 39 the number is a heart
       }
      
      else if (card2 > 39 && card2 <= 52){
        
          cardType2 = "Spades";//states that if the number is between 40 and 52 the number is a spade
      }
    
    //storing number for second card
    switch (card2 % 13){
            case 0:
             cardNumber2 = "King";
                break;
            case 1 :
             cardNumber2 = "Ace";
                break;
            case 2 :
            cardNumber2 = "2";
                break;
            case 3 :
            cardNumber2 = "3";
                break;
            case 4 :
            cardNumber2 = "4";
                break;
            case 5 :
            cardNumber2 = "5";
                break;
            case 6 :
            cardNumber2 = "6";
                break;
            case 7 :
            cardNumber2 = "7";
                break;
            case 8 :
            cardNumber2 = "8";
                break;
            case 9 :
             cardNumber2 = "9";
                break;
            case 10 :
             cardNumber2 = "10";
                break;
            case 11 :
             cardNumber2 = "Jack";
                break;
            case 12 :
             cardNumber2 = "Queen";
                break;
            default :
              System.out.println("");
                break;
    }
    
    
    //defines the suit of cardType3
    if (card3 <= 13){
        
          cardType3 = "Diamonds"; // states that if the number between 1 and 13 the number is a diamond
      }
       
      else if ( 13 < card3 && card3 <= 26 ){
          
          cardType3 = "Clubs";// states that if the number is between 14 and 26 the number is a club
      }
      
      else if ( 26 < card3 && card3 <= 39){
              
          cardType3 = "Hearts";//states that if the number is between 27 and 39 the number is a heart
       }
      
      else if (card3 > 39 && card3 <= 52){
        
          cardType3 = "Spades";//states that if the number is between 40 and 52 the number is a spade
      }
    
    
    //storing number for third card
    switch(card3 % 13 ) {
            case 0 :
             cardNumber3 = "King";
                break;
            case 1 :
             cardNumber3 = "Ace";
                break;
            case 2 :
            cardNumber3 = "2";
                break;
            case 3 :
            cardNumber3 = "3";
                break;
            case 4 :
            cardNumber3 = "4";
                break;
            case 5 :
            cardNumber3 = "5";
                break;
            case 6 :
            cardNumber3 = "6";
                break;
            case 7 :
            cardNumber3 = "7";
                break;
            case 8 :
            cardNumber3 = "8";
                break;
            case 9 :
             cardNumber3 = "9";
                break;
            case 10 :
             cardNumber3 = "10";
                break;
            case 11 :
             cardNumber3 = "Jack";
                break;
            case 12 :
             cardNumber3 = "Queen";
                break;
            default :
              System.out.print("");
                break;
    }
    
    //defines the suit of cardType4
    if (card4 <= 13){
        
          cardType4 = "Diamonds"; // states that if the number between 1 and 13 the number is a diamond
      }
       
      else if ( 13 < card4 && card4 <= 26 ){
          
          cardType4 = "Clubs";// states that if the number is between 14 and 26 the number is a club
      }
      
      else if ( 26 < card4 &&  card4 <= 39){
              
          cardType4 = "Hearts";//states that if the number is between 27 and 39 the number is a heart
       }
      
      else if (card4 > 39 && card4 <= 52){
        
          cardType4 = "Spades";//states that if the number is between 40 and 52 the number is a spade
      }
    
    //storing number for fourth card
    switch(card4 % 13 ) {
            case 0 :
             cardNumber4 = "King";
                break;
            case 1 :
             cardNumber4 = "Ace";
                break;
            case 2 :
            cardNumber4 = "2";
                break;
            case 3 :
            cardNumber4 = "3";
                break;
            case 4 :
            cardNumber4 = "4";
                break;
            case 5 :
            cardNumber4 = "5";
                break;
            case 6 :
            cardNumber4 = "6";
                break;
            case 7 :
            cardNumber4 = "7";
                break;
            case 8 :
            cardNumber4 = "8";
                break;
            case 9 :
             cardNumber4 = "9";
                break;
            case 10 :
             cardNumber4 = "10";
                break;
            case 11 :
             cardNumber4 = "Jack";
                break;
            case 12 :
             cardNumber4 = "Queen";
                break;
            default :
              System.out.print("");
                break;
    }
    
        //defines the suit of cardType5
    if (card5 <= 13){
        
          cardType5 = "Diamonds"; // states that if the number between 1 and 13 the number is a diamond
      }
       
      else if ( 13 < card5 && card5 <= 26 ){
          
          cardType5 = "Clubs";// states that if the number is between 14 and 26 the number is a club
      }
      
      else if ( 26 < card5 && card5 <= 39){
              
          cardType5 = "Hearts";//states that if the number is between 27 and 39 the number is a heart
       }
      
      else if (card5 > 39 && card5 <= 52){
        
          cardType5 = "Spades";//states that if the number is between 40 and 52 the number is a spade
      }
    
    //storing number for fifth card
    switch(card5 % 13 ){
            case 0 :
             cardNumber5 = "King";
                break;
            case 1 :
             cardNumber5 = "Ace";
                break;
            case 2 :
            cardNumber5 = "2";
                break;
            case 3 :
            cardNumber5 = "3";
                break;
            case 4 :
            cardNumber5 = "4";
                break;
            case 5 :
            cardNumber5 = "5";
                break;
            case 6 :
            cardNumber5 = "6";
                break;
            case 7 :
            cardNumber5 = "7";
                break;
            case 8 :
            cardNumber5 = "8";
                break;
            case 9 :
             cardNumber5 = "9";
                break;
            case 10 :
             cardNumber5 = "10";
                break;
            case 11 :
             cardNumber5 = "Jack";
                break;
            case 12 :
             cardNumber5 = "Queen";
                break;
            default :
              System.out.print("");
                break;
    }
      
    //telling the user what each of the random cards were
    System.out.println("Your random cards were:");
    
    System.out.println("The " + cardNumber1 + " of " + cardType1);
    
    System.out.println("The " + cardNumber2 + " of " + cardType2);
    
    System.out.println("The " + cardNumber3 + " of " + cardType3);
    
    System.out.println("The " + cardNumber4 + " of " + cardType4);
    
    System.out.println("The " + cardNumber5 + " of " + cardType5);
    
    
    //if statements for three of a kind
    //will tell you if the five random cards generated a three of a kind
  if  ((cardNumber1 == cardNumber2 && cardNumber3 == cardNumber1) || (cardNumber1 == cardNumber3 && cardNumber1 == cardNumber4) || (cardNumber1 == cardNumber3 && cardNumber1 == cardNumber5)){
        System.out.println("You have a three of a kind!");
    }
    
     else if ((cardNumber1 == cardNumber2 && cardNumber1 == cardNumber4) || (cardNumber1 == cardNumber4 && cardNumber1 == cardNumber5) || (cardNumber1 == cardNumber2 && cardNumber1 == cardNumber5)){
      System.out.println("You have a three of a kind!");
    }
    
     else if ((cardNumber2 == cardNumber3 && cardNumber2 == cardNumber4) || (cardNumber2 == cardNumber3 && cardNumber2 == cardNumber5) || (cardNumber2 == cardNumber4 && cardNumber2 == cardNumber5)){
      System.out.println("You have a three of a kind!");
    }
    
     else if (cardNumber3 == cardNumber4 && cardNumber3 == cardNumber5){
      System.out.println("You have a three of a kind!");
    }
    
    
    
    //tells user if they have a two pairs
    //checks all the possibilities, i know this could have been more efficient if i only created one math.Random and used that to lessen the variables of my code or use a counter but i was too far in by the time i was writing these possibilities
      else if ((cardNumber1 == cardNumber2 && cardNumber3 == cardNumber4) || (cardNumber1 == cardNumber2 && cardNumber3 == cardNumber5) || (cardNumber1 == cardNumber2 && cardNumber4 == cardNumber5)) {
      System.out.println("You have two pairs!");
    }
    
      else if ((cardNumber1 == cardNumber3 && cardNumber2 == cardNumber4) || (cardNumber1 == cardNumber3 && cardNumber2 == cardNumber5) ||(cardNumber1 == cardNumber3 && cardNumber4 == cardNumber5)) {
      System.out.println("You have two pairs!");
    }
    
      else if ((cardNumber1 == cardNumber4 && cardNumber2 == cardNumber3) || (cardNumber1 == cardNumber4 && cardNumber2 == cardNumber5) || (cardNumber1 == cardNumber4 && cardNumber3 == cardNumber5)){
      System.out.println("You have two pairs!");
    }
    
      else if ((cardNumber1 == cardNumber5 && cardNumber2 == cardNumber3) || (cardNumber1 == cardNumber5 && cardNumber2 == cardNumber4) || (cardNumber1 == cardNumber5 && cardNumber3 == cardNumber4)){
      System.out.println("You have two pairs!");
    }
    
      else if ((cardNumber2 == cardNumber3 && cardNumber4 == cardNumber5) || (cardNumber2 == cardNumber4 && cardNumber3 == cardNumber5) || (cardNumber2 == cardNumber5 && cardNumber3 == cardNumber4)){
      System.out.println("You have two pairs!");
    }
   
    //if statements to tell you if you have a pair
    //will tell the user if they have a two cards out of the five with the same number
      else if (cardNumber1 == cardNumber2) {
      System.out.println("You have a pair!");
    }
    
      else if(cardNumber1 == cardNumber3){
      System.out.println("You have a pair!");
    }
       
      else if (cardNumber1 == cardNumber4){
      System.out.println("You have a pair!"); 
    }
        
      else if (cardNumber1 == cardNumber5){
      System.out.println("You have a pair!");
    }
        
      else if (cardNumber2 == cardNumber3) {
     System.out.println("You have a pair!");
    }
       
      else if (cardNumber2 == cardNumber4){
     System.out.println("You have a pair!");
    }
        
      else if (cardNumber2 == cardNumber5){
      System.out.println("You have a pair!");
    }
    
      else if (cardNumber3 == cardNumber4){
      System.out.println("You have a pair!");
    }
    
      else if (cardNumber3 == cardNumber5){
      System.out.println("You have a pair!");
    }
         
      else if (cardNumber4 == cardNumber5){
      System.out.println("You have a pair!");
    }
        
        else {
      System.out.println("You have a high card!");
    }
    
    
  } //end of main method
    
} //end of class
  
  
  
  
  
  
  