//////////////////////////////////////
////// CSE2 PatternB
//// Rebecca Price   3/8/19
//// Creates a decreasing pyramid pattern based on a user's inputs

import java.util.Scanner; //call scanner
public class PatternB{
  public static void main (String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10"); // ask user for the input that will affect the length of the patter
   
    String junkWord;
    
       
  while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer 1 through 10
      
      System.out.println("Not an integer please enter a number 1 through 10");
      
      junkWord = myScanner.next();

    }
      int num = myScanner.nextInt(); //users input saved in num
    
   
  while (num > 10 || num < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Not an integer please enter a number 1 through 10");
      
      num = myScanner.nextInt();
        
      } 

    
    int length = num; //declaring variables to later be used in the loop
    int numRows;
    int i = 1; //declares i which will be used to decrease the number of length 
    for(numRows = 0; num >= numRows; numRows++ ){ //initial loop to ensure the number of lines in the pyramid
      for (i = num - 1; i >= numRows; i--){ //loop to increase the number within each line until numRows is equal to i 
        System.out.print(length - i + "");
      }
      System.out.println();
    }
    
   
    
    
    
    
    
    
    
    
    
  }
}
  