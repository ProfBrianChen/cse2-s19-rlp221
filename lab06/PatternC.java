//////////////////////////////////////
////// CSE2 PatternC
//// Rebecca Price   3/8/19
//// Creates a decreasing pyramid pattern based on a user's inputs but had it's height on the left side not the right


import java.util.Scanner; //call scanner
public class PatternC{
  public static void main (String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10"); // ask user for the input that will affect the length of the patter
   
    String junkWord;
    
        
  while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer 1 through 10
      
      System.out.println("Not an integer please enter a number 1 through 10");
      
      junkWord = myScanner.next();

    }
      int num = myScanner.nextInt(); //users input saved in num
    
   
  while (num > 10 || num < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Not an integer please enter a number 1 through 10");
      
      num = myScanner.nextInt();
        
      } 

    
    int length = num;
    int numRows;
    int spaces =  num - 1;
    int i = 0;

    for(numRows = 1; num >= numRows; numRows++ ){
      
      for (length = num; length > numRows; length--){ 
        System.out.print(" ");
      }
      
      for (i = 0; i < numRows; i++){
        System.out.print(length - i + "");
      }
      
      System.out.println();
    }
    
    
   
    
    
    
    
    
    
    
    
    
  }
}
  