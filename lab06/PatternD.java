//////////////////////////////////////
////// CSE2 PatternD
//// Rebecca Price   3/8/19
//// Creates a decreasing pyramid pattern based on a user's inputs


import java.util.Scanner; //call scanner
public class PatternD{
  public static void main (String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10"); // ask user for the input that will affect the length of the patter
   
    String junkWord;
    
    
  while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer 1 through 10
      
      System.out.println("Not an integer please enter a number 1 through 10");
      
      junkWord = myScanner.next();

    }
      int num = myScanner.nextInt(); //users input saved in num
    
   
  while (num > 10 || num < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Not an integer please enter a number 1 through 10");
      
      num = myScanner.nextInt();
        
      } 

    
    int length = num; //sets length equal to the users unput
    int numRows; //Declared numRows
    int i = 0; //initializes i
    for(numRows = 0; num >= numRows; numRows++ ){ //ensures the number of lines of the pattern are equal to the users input
      for (i = 0; length > i; i++){ //increases i everytime the loop runs therefore decreasing the number outputed onto the line until it equals 1
        System.out.print(length - i + "");
      }
      System.out.println();
      length--; //decreases length so the pattern will decrease the starting number in each line as it moves to the next line
    }
    
    
   
    
    
    
    
    
    
    
    
    
  }
}
  