/////////////////////////////////////
///// CSE 2 Pattern A
/// Rebecca Price    3/8/19
/// Make a pattern of increasing numbers and lines based on the input given by the user


import java.util.Scanner; //call scanner
public class PatternA{
  public static void main (String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter an integer between 1 and 10"); // ask user for the input that will affect the length of the patter
   
    String junkWord;
    
    
      
  while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer 1 through 10
      
      System.out.println("Not an integer please enter a number 1 through 10");
      
      junkWord = myScanner.next();

    }
      int num = myScanner.nextInt(); //users input saved in num
    
   
  while (num > 10 || num < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Not an integer please enter a number 1 through 10");
      
      num = myScanner.nextInt();
        
      } 
      
      
    
 
    
    int length; 
    int numRows;
    for(numRows = 1; num >= numRows; numRows++ ){
      for (length = 1; length <= numRows; length++){
        System.out.print( length + "");
      }
      System.out.println();
      length = 0;
    }
    
   
    
    
    
    
    
    
    
    
    
  }
}
  