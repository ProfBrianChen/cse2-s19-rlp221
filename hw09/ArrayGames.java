////////////////////////////////
///// CSE2 ArrayGames
/// Rebecca Price   4/14/19

import java.util.Scanner;
public class ArrayGames{
   public static void main(String args[]){
      Scanner myScanner = new Scanner(System.in);
      
      System.out.println("Do you want to run insert or shorten?");
      
     String userChoice = myScanner.next();
      
      if(userChoice.equals("insert")){
         int[] array1 = generate();
         int[] array2 = generate();
         array1 = print(array1); //set the values created in print to the array
         array2 = print(array2); //set the values created in print to the array
         insert(array1, array2); //call the array the will put the two arrays together
         
      }
      
      if(userChoice.equals("shorten")){
         int[] array1 = generate(); 
         int num = (int) (Math.random()*30); //generate a random number that can be outside of the arrays index. anywhere from 0 to 30
         array1 = print(array1); //set the values created in print to the array
         shorten(array1, num);
      }
      
   }
   
   public static int[] generate(){
      int num = (int) (Math.random()*10 + 10); //creates a random integer between 10 and 20
      int[] generateArray = new int[num]; //creates array with memory space based on the random integer
      return generateArray; //return the pointer to the array
   }
   
   public static int[] print(int[] generate){
      int length = generate.length; //find length of the array 
      
      System.out.print("Input: ");
      for(int i = 0; i < length - 1; i++){ //print the inputs of the array
         generate[i] = (int) (Math.random()* 50 + 1);
        
         System.out.print(generate[i] + " ");
      }
      System.out.println();
      return generate;
   }
   
   public static void insert(int [] array1, int [] array2){
      int lengthArray1 = array1.length; //length of the first array
      int lengthArray2 = array2.length; //length of the second array
      int length = array1.length + array2.length; //create a length that combines both of the arrays
      int[] newArray = new int[length]; //create new array
      int randomMember = (int) (Math.random()*lengthArray1 + 1); //find a random
      
      System.out.print("Output: ");
      for(int i = 0; i < randomMember - 1; i++){ //print the beginning of the first array up until the randomMember
         newArray[i] = array1[i];
         System.out.print(newArray[i] + " ");
      }
      
      for(int i = 0; i < lengthArray2 - 1; i++){ //print the rest of the array2
         newArray[i+randomMember] = array2[i];
         System.out.print(array2[i] + " ");
      }
      
      for(int i = randomMember; i < lengthArray1 - 1; i++){ //prints the other half of the first input
         newArray[i + lengthArray2] = array1[i];
         System.out.print(array1[i] + " ");
      }
   }
   
   public static int[] shorten(int[] array, int input){ //used to shorten array if the input integer is within the range
      System.out.println("Input2: " + input);
      int length = array.length - 1; //adjusts length for delted input member
      int[] newArray = new int[length]; //create new array that will be adujusted for deleting the input member
      if(input < array.length){
         System.out.print("Output ");
         for(int i = 0; i < input; i++){ //prints the array up until the deleted number and saves it to the adjusted arraay
            newArray[i] = array[i];
            System.out.print(newArray[i] + " ");
         }
         for(int i = input; i < length - 1; i++){
            newArray[i] = array[i + 1];
            System.out.print(newArray[i] + " ");
         }
         return newArray;
      }
      
      else{
         System.out.print("Output: ");
         for(int i = 0; i < array.length - 1; i++){
            System.out.print(array[i] + " ");
         }
         return array;
      }
   }
}