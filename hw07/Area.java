///////////////////////////////////
////// CSE2 Area
///// Rebecca Price    3/22/19
// calculates the area of different shapes

import java.util.Scanner;
public class Area{


    public static double getTriangleArea(double base, double height){ //method for triangle
    
      double triangleArea = base * height * .5; //calculates triangle area
      
      return triangleArea;
    }
  
    public static double getCircleArea(double radius1, double radius2){ //calculates the area of the circle given the user inputs
      
      double circleArea = 3.14 * radius1 * radius2;
      
      return circleArea;
      
    }
      
    public static double getSquareArea(double length, double width){ //calculates the area of the square given the user inputs
      
      double squareArea = length * width;
      
      return squareArea;
      
    }
  
    public static double checkInputs(double x, double y) { //check that users inputs are valid and are doubles  FIX
     
      Scanner myScanner = new Scanner(System.in);
      
      while(x < 0){
        
        System.out.println("Error: please enter a double");
        
        x = myScanner.nextDouble();
       
      }
      
      while (y < 0){
        
        System.out.println("Error: please enter a double");
        
        y = myScanner.nextDouble();
        
      }
      
      return x;
    }
  
    public static void main(String args[]){
        
          Scanner myScanner = new Scanner (System.in); 

          String junkWord;

          System.out.println("Which shape would you like to calculate: circle, triangle, or square? (please enter each shape with all lower cases)"); //asks user to pick a shapre

          while(myScanner.hasNext() == false){ //checks users inputs, if they do not match, then it tells the user there is an error and asks the user for an input until it is one of the specified shapes

            System.out.println("Error: not a valid shape. Please enter circle, triangle, or square");

            junkWord = myScanner.next();
          }

          String shape = myScanner.next();
          

         

         while (!shape.equals("circle") && !shape.equals("triangle") && !shape.equals("square")){

            System.out.println("Error: please pick one of the specified shapes and type them with all lower cases");

            shape = myScanner.next();

          }
       
      
      if(shape.equals("triangle")){ //asks for user for inputs
         
        System.out.println("Please pick the base of the triangle");
        
        double base = myScanner.nextDouble();
        
        System.out.println("Please pick the height of the triangle");
        
        double height = myScanner.nextDouble();
          
        double inputs = checkInputs(base,height); //calls method to check the inputs 
        
        double triangleArea = getTriangleArea(base,height); //calls method to calculate the area
        
        System.out.println("The area of the triangle is " + triangleArea); //prints area
      }
      
      if(shape.equals("circle")){ //asks for user for inputs
         
        System.out.println("Please pick the radius of the circle");
        
        double radius1 = myScanner.nextDouble();
        
        double radius2 = radius1;
        
        double inputs = checkInputs(radius1, radius2); //calls the method to check the inputs
        
        double circleArea = getCircleArea(radius1,radius2); //calls method to calculate the area
        
        System.out.println("The area of the cirlce is " + circleArea); //prints area
        
      }
      
      if(shape.equals("square")){ //asks for user for inputs
         
        System.out.println("Please pick the length of the square");
        
        double length = myScanner.nextDouble();
        
        System.out.println("Please pick the height of the square");
        
        double height = myScanner.nextDouble();
          
        double inputs = checkInputs(length,height);
        
        double squareArea = getSquareArea(length, height);
        
        System.out.println("The area of the square is " + squareArea); //prints area
      }
    
    }
}