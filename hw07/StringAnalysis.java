////////////////////////////////
////// CSE2  StringAnalysis
/// rebecca price .   2/24/19
/// ask user if they would like to examine a string by all the characters or certain number of charcters in the string

import java.util.Scanner;
public class StringAnalysis{
  
  public static boolean checkForLetters(String userWord,int numberOfCharacters){
    
    numberOfCharacters = numberOfCharacters - 1;
    
    int charactersInWord = userWord.length(); //figure out how many charcters in word
      
    char character;
    
    boolean isLetter = true;
    
    while(numberOfCharacters > 0 && numberOfCharacters <= charactersInWord){ //check if each character is letter based on the users input 
      
      character = userWord.charAt(numberOfCharacters);
      
      isLetter = Character.isLetter(character);
      
      numberOfCharacters--;

    }
    
    return isLetter; //reutrn true if it is a letter
    
  }
  
  public static boolean checkForLetters(String userWord){ //check if every character within the word is a letter 
    
    int charactersInWord = userWord.length();
    
    int numberOfCharacters = charactersInWord -1 ;
    
    char character;
    
    boolean isLetter = true;
    
    while(charactersInWord > 0){
    
      character = userWord.charAt(numberOfCharacters);
      
      isLetter = Character.isLetter(character);
      
      charactersInWord--;

    }
    
    return isLetter; //return true if they are letters
    
  }
  
  public static void main (String args[]) {
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println("Please type in a word");
    
    String junkWord;
    
      while(myScanner.hasNext() == false){ //checks to make sure the user entered a word

        System.out.println("Error: please type in a word");
        
        junkWord = myScanner.next();
        
      }
    
   String userWord = myScanner.next();
    
    System.out.println("Would you like to examine all the characters of the word or a certain number of characters? If you want to examine all type the number 1, if you want to examine a certain number type the number 2");
    
    while(myScanner.hasNextInt() == false){ //check if user entered a numbers which will define what type of method to run
      
      System.out.println("Error: please answer with the number 1 or 2");
      
      junkWord = myScanner.next();
    }
    
    int userChoice = myScanner.nextInt();
    
    while(userChoice < 1 || userChoice > 2){ //checks to make sure user inputs is a 1 or a 2
      
      System.out.println("Error: please answer with the number 1 or 2");
      
      userChoice = myScanner.nextInt();
      
    }
    
    if(userChoice == 2){ //defines to use the method that asks user how many characters to check and then checks to make sure they give a specified number
      
      System.out.println("How many characters would you like to check");
      
      while(myScanner.hasNextInt() == false){
        
        System.out.println("Error please enter a number");
        
        junkWord = myScanner.next();
        
      }
      
      int numberOfCharacters = myScanner.nextInt();
      
      while(numberOfCharacters < 0){ //checks users input to be a positive number
        
        System.out.println("Error: please enter a positive number");
        
        numberOfCharacters = myScanner.nextInt();
      }
      
      boolean letters = checkForLetters(userWord,numberOfCharacters);
      
      System.out.println(letters);
    }
    
    if(userChoice == 1){ //calls method that will evaluate all characters within the string
      
      boolean letters = checkForLetters(userWord);
      
      System.out.println(letters);
      
    }
    
    
    
    
    
  }
}