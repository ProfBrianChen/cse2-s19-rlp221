/////////////////////////////
//// CSE2  Searching
/// Rebecca Price    4/12/19

import java.util.Scanner;

public class Searching{
   
   public static void main(String args[]){
      
      Scanner myScanner = new Scanner (System.in);
      
      System.out.println("Would you like to do a linear or a binary search?");
      
      String userChoice = myScanner.next();
      
      if(userChoice.equals("binary")){ //if they answer binary this will ask for the size and search integer they want
         System.out.println("What array size would you like?");
         int arraySize1 = myScanner.nextInt();
         System.out.println("What search number would you like");
         int searchInt1 = myScanner.nextInt();
         
         int[] array2 = getArray2(arraySize1);
         
         for(int i = 0; i < arraySize1; i++){ //print the array
            System.out.print(array2[i] + " ");
         }
         System.out.println();
         
         int last = array2.length - 1; //find the length of the array to be used when doing the binary search
         
         getTerm1(searchInt1, array2, 0, last);
         
      }
      
      if(userChoice.equals("linear")){//if they answer linar this will ask for the size and search integer they want
         System.out.println("What array size would you like?");
         int arraySize = myScanner.nextInt();
         System.out.println("What search number would you like");
         int searchInt = myScanner.nextInt();
         
         int[] array1 = getArray1(arraySize);//calls the method that will generate the array
         
         for(int i = 0; i < arraySize; i++){ //prints the values of that array
            array1[i] = i + (int) (Math.random()*10 + 1);
            System.out.print(array1[i] + " ");
         }
         System.out.println();
         
         int index1 = getTerm(searchInt, getArray1(arraySize));//calls the method that will output the index for the search term and saves it in index
         System.out.println("The index where the search term can be found is " + index1);//prints the index value
      }
      

   }
   
   public static int[] getArray1(int size){ //method for the linear array, creates an array based on the users inputs
      int[] regularArray = new int[size];
      
      for(int i = 0; i < size - 1; i++){
         regularArray[i] = i + (int) (Math.random()*10 + 1);
      }
     return regularArray; //returns the array
   }
   
   public static int getTerm(int searchTerm, int[] arraySearch){ //method for linear array that will find the index based on the users inputs
      int length = arraySearch.length; //finds length of the array
      
      for(int i = 0; i < length; i++){ //checks to see if the search number is within the array and if so prints the index of that number
         if(arraySearch[i] == searchTerm){
            return i;
         }
      }
      return -1;   //returns -1 if the search term is not found in the array
   }
   
   public static int[] getArray2(int size){ //method fo the binary array, creates the array based on users inputs and sorts it so that it randomly ascends
      int[] array = new int[size];
      
      for(int i = 0; i < size ; i++){
         if(i > 0){
            array[i] = array[i-1] + (int) (Math.random()*10 + 1);
         }
         else{
            array[i] = (int) (Math.random()*10 + 1);
         }
      }
      
      return array;
   }
   
   public static void getTerm1(int searchTerm1, int[] arraySearch1, int first, int last){
      int length = arraySearch1.length;
      int mid = (first + last) / 2;
      while(first <= last){
         if(arraySearch1[mid] < searchTerm1){
            first = mid + 1;
         }
         if(searchTerm1 == arraySearch1[mid]){
            System.out.println("The index where the search term can be found is " + mid);
            break;
         }
         if(searchTerm1 < arraySearch1[mid]){
            last = mid - 1;
         }
         mid = (first + last)/2;
        }
      if(first > last){
            System.out.println("-1");
      }
   }
}