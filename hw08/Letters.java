/////////////////////////////
/////// CSE2 hw08
///// Rebecca Price 4/5/19
/// split an array into two arrays based on the letters

import java.util.Random; 
import java.util.Arrays;

public class Letters{
   public static void main (String args[]){
      
      int randomSize = (int) (Math.random() * 50 + 1); //create a random size varying between 0 and 100
      
      System.out.println(randomSize);
      
      char[] randomArray = new char[randomSize]; //points the array towards the randomly generated number and sets that as the arbitrary size of the array
      
      char base = 'a';
      
      System.out.print("Random character array: "); 
      
      for(int i = 0; i < randomSize; i++){ //initialize each possible value of the array to a random character a to z and A to Z
         int num = (int) (Math.random() * 52);
         if (num < 26){ //ensures it will only print letters not other characters, creates a base so only letters will be stored
           base = 'A';
         }
         else{
           base = 'a';
         }
         randomArray[i] = (char) (base + num % 26); //stores value of the randomly generated char into the next value of the array
         
         System.out.print(randomArray[i]); //prints each value of the array
      } 
      System.out.println(); //prints a space between each array so that it will be easier to read
     
      getAtoM(randomArray);
      
      getNtoZ(randomArray);
   }
   
   public static void getAtoM(char[] AtoMArray){
      int length = AtoMArray.length; //find the length of the way
      System.out.print("AtoM characters: ");
      for(int i = 0; i < length; i++){ //loops to check whether or not the values are between a and m or A and M
         char letter = AtoMArray[i];
         
         if(letter >= 'A' && letter < 'N' || letter >= 'a' && letter < 'n'){ //prints uppercase and lowercase letters that are A through M and 
            AtoMArray[i] = letter;
            System.out.print(AtoMArray[i]);
         }
         
      }
      System.out.println();
   }
   
    public static void getNtoZ(char[] NtoZArray){
      int length = NtoZArray.length; //find the length of the way
      System.out.print("NtoZ characters: ");
      for(int i = 0; i < length; i++){ //loops to check whether or not the values are between N and Z
         char letter = NtoZArray[i];
         if(letter > 'M' && letter <= 'Z' || letter <= 'z' && letter > 'm'){ //prints uppercase and lowercase letter through Z and N 
            NtoZArray[i] = letter;
            System.out.print(NtoZArray[i]);
         } 
      }
      System.out.println();
   }
}




















