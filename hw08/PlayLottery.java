//////////////////////////
/// CSE2 Play Lotters
/// 4/7/19  Rebecca price
///help the user play the lottery

import java.util.Scanner;

public class PlayLottery{
   public static void main (String args[]){
      
      Scanner myScanner = new Scanner(System.in);
      
      int[] userNumbers = new int[5]; //creates array that can hold 5 ints
      
      for(int i = 0; i < 5; i++){
         System.out.print("Enter a number between 0 and 59: "); //ask user for number
         userNumbers[i] = myScanner.nextInt();
      }
      
      System.out.print("The winning lottery numbers are: ");
      
      int[] lotteryNumbers = numbersPicked(); //calls for the method in which these numbers are created
      for(int i = 0; i < 5; i++){
         System.out.print(lotteryNumbers[i] + " "); //prints out the values 
      }
      System.out.println();
      boolean winOrLose = userWins(userNumbers, lotteryNumbers);
      if(winOrLose == true){
         System.out.println("You won!");
      }
      if(winOrLose == false){
         System.out.println("You lose");
      }
   }
   
   public static int[] numbersPicked(){
     
     int[] lotteryNumbers = new int[5]; //created an arraythat will hold 5 values, these values will be randomly generated 
      
      for(int i = 0; i < 5; i++){ //generate 5 random numbers, without replacement, that will be used for the lotteryNumbers
         
        lotteryNumbers[i] = (int) (Math.random() * 59 + 1);
         
       for(int j = 0; j < i; j++){
          if(lotteryNumbers[i] == lotteryNumbers[j]){
             i--;
             break;
          }
       }
                  
     }
     
      return lotteryNumbers;
      
   }
   
   public static boolean userWins(int[] user, int[] winning){
      
      if(user[1] == winning[1] && user[2] == winning [2] && user[3] == winning[3] && user[4] == winning[4] && user[0] == winning[0]){
         return true;
      }
      else{
         return false;
      }
      
   }
   
}