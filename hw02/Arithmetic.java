/////////////////////////////
///CSE2 Rebecca Price 2/1/19
///computes the cost of all the items I bought at the store
public class Arithmetic {
    public static void main(String args[]) {
        
        //number of pairs of pants
        int numPants = 3; 
        
        //cost per pair of pants
        double pantsPrice = 34.98;
        
        //number of sweatshirts
        int numShirts = 2;
        
        //Cost per shirt
        double shirtPrice = 24.99;

        //Number of belts
        int numBelts = 1;
        
        //cost per belt
        double beltCost = 33.99;
 
        //the tax rate
        double paSalesTax = 0.06;
        
        //Total cost of each item
        double totalCostPants = numPants * pantsPrice; //total cost of pants
        
        double totalCostShirts = numShirts * shirtPrice; //total cost of shirts
        
        double totalCostBelts = numBelts * beltCost; //total cost of belts
              
        //Sales tax charged by buying each item
        double salesTaxPants = (int) (paSalesTax * totalCostPants * 100);
      
                  salesTaxPants = salesTaxPants/100; //sales tax on the pants
          
        double salesTaxShirts = (int) (paSalesTax * totalCostShirts * 100); 
                  
                  salesTaxShirts = salesTaxShirts/100; //sales tax on the shirts
         
        double salesTaxBelts = (int) (paSalesTax * totalCostBelts * 100); 
      
                  salesTaxBelts = salesTaxBelts/100; //sales tax on the belts
      
        //total cost of purchases before tax
        double totalCostOfPurchasesBeforeTax = totalCostBelts + totalCostPants + totalCostShirts;
          
        //total sales tax 
        double totalSalesTax = (int) (paSalesTax * totalCostOfPurchasesBeforeTax * 100);
                  
                  totalSalesTax = totalSalesTax/100;
      
          //prints the total cost and sales tax for each of the items purchased
          System.out.println("The total cost of the belts is " + totalCostBelts + " and the total cost of the belts sales tax is " + salesTaxBelts + "."); //total cost and sales tax for the belts
            
          System.out.println("The total cost of the pants is " + totalCostPants + " and the total cost of the pants sales tax is " + salesTaxPants + "."); //total cost and sales tax for the pants
            
          System.out.println("The total cost of the shirts is " + totalCostShirts + " and the total cost of the shirts sales tax is " + salesTaxShirts + "."); //total cost and sales tax for the shirts
      
          //prints the total sales tax
          System.out.println("The total sales tax for the purchases is " + totalSalesTax + ".");
      
          //prints the total cost of the purchases before the sales tax
          System.out.println("The total cost of the purchases before the sales tax is " + totalCostOfPurchasesBeforeTax);
      
        //total cost of purchases including tax 
        double totalCostOfPurchases = totalCostOfPurchasesBeforeTax + totalSalesTax;
          
          //total cost of the purchases including the sales tax
          System.out.println("The total cost of the puchases, including tax, is " + totalCostOfPurchases + ".");
    }//end of main method
}//end of class
          
          