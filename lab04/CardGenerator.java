////////////////////////////////////
///// CSE2 Card Generator 
/// Rebecca Price 2/15/19
/// program used to randomly generate a card and state its suit and number

public class CardGenerator{
  
    public static void main (String args[]){
  
  int randomNumber = (int) (Math.random()* 52); //value of randomNumber is now randomly picked from anywhere between 0 and 52   
        
      String cardType = "card";
      String cardNumber = "card";
        
      System.out.println(randomNumber); //prints the actual random number
      
      if (randomNumber <= 13){
        
          cardType = "Diamonds"; // states that if the number between 1 and 13 the number is a diamond
      }
       
      else if ( 13 < randomNumber && randomNumber <= 26 ){
          
          cardType = "Clubs";// states that if the number is between 14 and 26 the number is a club
      }
      
      else if ( 26 < randomNumber && randomNumber <= 39){
              
          cardType = "Hearts";//states that if the number is between 27 and 39 the number is a heart
       }
      
      else if (randomNumber > 39 && randomNumber <= 52){
        
          cardType = "Spades";//states that if the number is between 40 and 52 the number is a spade
      }
     
      
      int number = randomNumber % 13;  // takes the remainder of the randomNumber divided by 13, uses that to determine the number of the card in the switch 
          switch (number)      {
            case 0 :
             cardNumber = "King";
                break;
            case 1 :
             cardNumber = "Ace";
                break;
            case 2 :
            cardNumber = "2";
                break;
            case 3 :
            cardNumber = "3";
                break;
            case 4 :
            cardNumber = "4";
                break;
            case 5 :
            cardNumber = "5";
                break;
            case 6 :
            cardNumber = "6";
                break;
            case 7 :
            cardNumber = "7";
                break;
            case 8 :
            cardNumber = "8";
                break;
            case 9 :
             cardNumber = "9";
                break;
            case 10 :
             cardNumber = "10";
                break;
            case 11 :
             cardNumber = "Jack";
                break;
            case 12 :
             cardNumber = "Queen";
                break;
            default :
              System.out.print("Invalid card");
                break;
          }
      
        System.out.println( "You picked the " + cardNumber + " of " + cardType);//print which card you picked
      
    }//end of main method
  
  }//end of class