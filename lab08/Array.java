//////////////////////////////////
///// CSE2 Rebecca Price
///// 4/5/19
/// randomly generate a number between 50 and 100, use that number to create an array and then calculate the range, mean, and standard deviance
import java.util.Arrays;

public class Array{
   public static void main (String args[]){

    int num = (int) (Math.random() * 50 + 51); //creates a random value between 0 and 100
      
      System.out.println(num);
      
    int[] numArray = new int[num]; //declares the array 
      
    System.out.println(numArray); //prints the array
      
      int i = 0;
      
      for(i = 0; i < num; i++){ //initializes each entry of the array
         numArray[i] = i;
         System.out.println("numArray: " + numArray[i]);
      }
 
       //calls method getRange and enter the array to be used
       int range = getRange(numArray);
       System.out.println("The range is " + range);
      
       // calls method getMean and enters the array to be used
       int mean = getMean(numArray);
       System.out.println("The mean is " + mean);
      
       //calls method detStdDev and enters the array to be used
       double StdDev = getStdDev(numArray);
       System.out.println("The Standard Deviation is " + StdDev);
      
       shuffle(numArray);
      
   }
   
   public static int getRange(int[] range){
      int length = range.length-1; //finds the maximum value

      Arrays.sort(range); //sorts the range
      
      int rangeValues = range[length] - range[0]; //calculates range of values
      
      return rangeValues;
      
   }
   
   public static int getMean(int[] meanArray){
      
      int length = meanArray.length-1; //defines length of the array
      
      int meanTotal = 0; 
        
      for(int i = 0; i < length; i++){ //adds each value of the array
         meanTotal += meanArray[i];
      }
      
      int mean = meanTotal/ (length + 1); //calculates the mean
      
      return mean;
   }
   
   public static double getStdDev(int[] StdDevArray){ //calculates the standard deviation 
      int mean = getMean(StdDevArray);
      
      int length = StdDevArray.length-1;
      
      int totalValue = 0;
      
      for(int i = 0; i < length; i++){ //finds the sum of the deviations from the mean
         int value = (i - mean)*(i-mean);
         totalValue += value;
      }
      
      double StdDev = (totalValue/length); 
      StdDev = Math.sqrt(StdDev); //find final value of the standard deviation
      
      return StdDev;
   }
   
   public static void shuffle(int[] shuffleArray){
      for (int i=0; i<shuffleArray.length-1; i++) { //finds a random number to swap with 
	
	         int target = (int) (shuffleArray.length * Math.random() );

	         int temp = shuffleArray[target]; //swaps the values
         
	         shuffleArray[target] = shuffleArray[i];
         
	         shuffleArray[i] = temp;
         
            System.out.println("numArray" + i + ": " + shuffleArray[i]);
     }
   }
 }
 