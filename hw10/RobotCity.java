////////////////////////////////////
///// CSE2 RobotCity
//// Rebecca Price  4/22/19
/// create a given block with populations, then randomly invade those blocks and udpate them so they shift eastward
// meant to help us practice multidimensional arrays

public class RobotCity{
   public static void main(String args[]){
     for(int i = 0; i < 1; i++){  //change this to repeating 5 times
         int[][] theCity = buildCity();
         display(theCity);
         int numOfRobots = (int) (Math.random()*10 + 1); //create random number of robots to invade 
         int[][] robotArray = invade(theCity, numOfRobots); //call the method robot array, to place the robots on random blocks
         System.out.println("------------------------------------------------------------------");
         display(robotArray); //display the updated city
         robotArray = update(robotArray); //update them eastward
         System.out.println("------------------------------------------------------------------");
         display(robotArray); //display the updated city
     } 
      
   }
   
   public static int[][] buildCity(){
      int NS = (int) (Math.random()*5 + 10); //assigns dimensions for NS
      int EW = (int) (Math.random()*5 + 10); //assigne dimensions for EW
      int[][] city = new int[NS][EW]; //creates the array with memory needed for the dimensions
      
      for(int i = 0; i < NS; i++ ){ //initialles all the variables
         for(int j = 0; j < EW; j++){
            city[i][j] = (int) (Math.random()* 899 + 100); //states the population for each of the blocks
         }
      }
      return city; 
   }
   
   public static void display(int[][] city1){ //prints the cities block populations
      int NS = city1.length;
      int EW = city1[0].length;
      for(int i = 0; i < NS; i++ ){ 
         for(int j = 0; j < EW; j++){
            System.out.printf("%4d ", city1[i][j]); //how do i print using a printf statement? FIXXXXX
         }
         System.out.println();
      }
   }
   
   public static int[][] invade(int[][] invadedArray, int k){ //create the coordinates in which the robots will invade
      int length2 = invadedArray[0].length; //find length of the array
      int length1 = invadedArray.length; //find length of the array
      int value = 0;
      for(int i = 0; i < k; i++){
         int num1 = (int)(Math.random()*(length1-1) + 1); //find the block 
         int num2 = (int)(Math.random()*(length2-1) + 1); //find the block in which the robot will invade
         value = invadedArray[num1][num2];
         invadedArray[num1][num2] = -value; //set value where the robot invaded to a negative value
         if(invadedArray[num1][num2] > 0){ //check if the value has already been invaded, if so resets the number to a negative value and will rerun the code 
            i--; //ensures that there will be the number of invasions equal to k
            invadedArray[num1][num2] = -value;
         }
      }
      return invadedArray;
   }
   
   public static int[][] update(int[][] city2){ //moves the robots, if they exist, to the east, FIXXXXX
      int length = city2.length;
      int memberLength = city2[0].length;
      for(int i = 0; i < length; i++){
         for(int j = memberLength-1; j >= 0; j--){
            if(city2[i][j] < 0 && j < memberLength - 2){ //if the value of the member is negative and the block is able to be moved twice to the east then move the value twice to the east
               city2[i][j+2] = -city2[i][j+2]; //fix
               city2[i][j] = -city2[i][j];
            }
            else if(city2[i][j] < 0 && j >= memberLength - 2){ //if the the block has a robot but if moved east will no longer be on the grid, the robot will disappear
               city2[i][j] = -city2[i][j]; //value becomes positive to represent there is no longer a robot on that block
            }
         }
      }
      return city2;
   }
}