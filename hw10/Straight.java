///////////////////////////////////////
///////  CSE2   Straight
/// Rebecca Price    4/28/19
/// Calculate the percentage of straights one will receive if they picked 5 cards from a randomly shuffled deck 1 million times

public class Straight{
   public static void main(String args[]){
     
     double counter = 0.0; //counts how many times the cards will produce a straight
      for(int n = 0; n < 1000000; n++){ //will test this a million times
     int[] shuffledArray = shuffle(); //calls the shuffled method to shuffle the array
     int[] cards = draw(shuffledArray); //uses the shuffled array to find the first five numbers
       for(int i = 0; i < 5; i++){ 
         switch(cards[i] % 13 ) { //define the numbers as a card numbers
            case 0 :
             cards[i] = 13;
                break;
            case 1 :
             cards[i] = 1;
                break;
            case 2 :
             cards[i] = 2;
                break;
            case 3 :
            cards[i] = 3;
                break;
            case 4 :
            cards[i] = 4;
                break;
            case 5 :
            cards[i] = 5;
                break;
            case 6 :
            cards[i] = 6;
                break;
            case 7 :
            cards[i] = 7;
                break;
            case 8 :
            cards[i] = 8;
                break;
            case 9 :
             cards[i] = 9;
                break;
            case 10 :
             cards[i] = 10;
                break;
            case 11 :
             cards[i] = 11;
                break;
            case 12 :
             cards[i] = 12;
                break;
            default :
              System.out.print("");
                break;
         }
       }
     boolean TF = search(cards); //calls the search method that will state whether or not the card combonations make a straight

     if(TF == true){ //if search returns true add 1 to the counter
        counter++; 
     }
      
   }
    System.out.println((counter/1000000)*100); //find the percentage that the method will create a straight
  }
   
   public static int[] draw(int[] array){ //creates an array of the first five cards of the shuffled array
       int[] first5 = new int[5];
      for(int i = 0; i < 5; i++){
         first5[i] = array[i];
      }
      return first5;
   }
   
   public static int[] shuffle(){ //shuffle the deck and ensure that no card is the same as another
      int[] shuffled = new int[52];
      for(int j = 0; j < 52; j++){ //initializes all the variables first so they can later be swapped
         shuffled[j] = j+1;
      }
      for(int i = 0; i < 52; i++){ //shuffles the numbers enough time to make sure the deck is rightfully shuffled
         int num = (int) (Math.random()*52); //find two indexes to switch
         int num1 = (int) (Math.random()*52);
         
         int temp = shuffled[num]; //swaps the two variables by creating a temporary value 
         shuffled[num] = shuffled[num1];
         shuffled[num1] = temp;
         }
      return shuffled; //return the shuffled array
      }
      
   
     
   public static int find(int[] array1, int k){
      if(k > 5 || k < 0){
         return -1;  //-1 will mean that there is an error
      }
      
      int min = array1[0]; //find the current 0th value of the array to compare to other values
      int minIndex = 0;
      for(int j = 0; j < k; j++){ //find the min of the kth number
         for(int i = j; i < array1.length; i++){ //check the rest of numbers within the array that have not already been sorted
            if(array1[i] < min){
               min = array1[i];
               minIndex = i;
            }
            if(i == array1.length - 1){ //swap the values if the lowest number is not 0th number and therefore we can find the kth lowest number
               int temp = array1[j];
               array1[j] = array1[minIndex];
               array1[minIndex] = temp;
            }
         }
         min = 1000; //resets min that it cannot be reached 
      }
      return array1[k-1]; //return lowest kth value wanted
   }
   
   public static boolean search(int[] array2){
        for(int i = 0; i < 5; i++){
           array2[i] = find(array2, (i+1)); 
           //System.out.print(array2[i] + " ");
        }
      //System.out.println();
        if(array2[0] == array2[1] - 1 && array2[0] == array2[2]-2 && array2[0] == array2[3] - 3 && array2[0] == array2[4] - 4){
           return true; //if they produce a straight then return boolean true
        }
        return false; //else return false
     }
      
   
}