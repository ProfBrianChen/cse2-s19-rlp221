////////////////////////////
////// CSE2 Check 
////// Rebecca Price 2/8/19
/// Allows you to inut the percentage of tip a group wishes to pay,the number of people, and the total cost of the check
/// Determines the amount of the check each person will pay

import java.util.Scanner;
//imports oringinal data

public class Check{
    			//main method required for every Java program
   			public static void main(String[] args) {
          
          //creating an instance that will take input from STDIN
          Scanner myScanner = new Scanner( System.in );
          
          //asks you to state the cost of the check before tip
          System.out.print("Enter the original cost of the check in the form of xx.xx: ");
          
             //set checkCost as a double and calls the myScanner to use the next double
            double checkCost = myScanner.nextDouble();
          
          //asks you to state the percentage of the tip you want as a whole number
          System.out.print("Enter the percentage tip you wish to pay as a whole number (in form of xx): ");
          
            double tipPercent = myScanner.nextDouble();
          
            //converts tip percent into a decimal value
            tipPercent /= 100; 
            
          //asks you to state how many poeple went out to dinner
          System.out.print("Enter the number of people who went out to dinner: ");
          
            int numPeople = myScanner.nextInt();
          
            double totalCost;
            
            double costPerPerson;
          
            //whole dollar amount of cost, for storing digits to the right of the decimal point, for the cost$
            int dollars, dimes, pennies;
          
            //calculates the total cost of the of the check
            totalCost = checkCost * (1 + tipPercent);
          
            //get the whole amount per person, dropping decimal fraction
            costPerPerson = totalCost / numPeople;
          
            //get dollars amount per person as an integer
            dollars = (int)costPerPerson;
          
            //get dimes amount per person 
            dimes = (int)(costPerPerson * 10 ) % 10;
          
            //get pennies amount per person
            pennies = (int)(costPerPerson * 100) % 10;
          
          //prints the cost of the check for each person
          System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

        }//end of main method   
  	} //end of class
