///////////////////////////////////
//// CSE2 Twist Generator
//// Rebecca Price .  3/1/19
/// asks user for an input of a number which will then be used to output a twist pattern
import java.util.Scanner;
  public class TwistGenerator {
    public static void main (String args []){
    
    Scanner myScanner;//calls myScanner
    
    myScanner = new Scanner(System.in);
    
    System.out.println("Please enter a positive integer."); // asks user to enter an integer
    
    String junkWord; //declaring an integer so it can later be used to reset myScanner if the user did not input an integer
      
    while (myScanner.hasNextInt() == false) { //while loop to see if the input was actually an integer, if not it will prompt the user for another inte
      
           System.out.println("Please enter a positive integer.");
      
           junkWord = myScanner.next(); //resets myScanner
      
           //myScanner.nextInt();
            
        } 
      
      int length = myScanner.nextInt(); //stores the correct integer into length
    
      int pattern = length % 3;
      length = length / 3;
      String top = "\\ /";
      String middle = " X ";
      String bottom = "/ \\";
      int i = 1, j = 1; //i creates a limit for the loop, it will continue adding and running the loop until it is more than the length integer 
         
          while(length >= i) { //will continue printing the pattern until i is bigger than length 
               System.out.print(top);
               i++;
           if(length < i && pattern <= 2){
              System.out.print("\\"); //there are two cases where the pattern will not perfectly fits in 3's, this will ensure the pattern in correct
              } 
           } 
           i=1;
           System.out.println();
      
          while(length >= i){ //prints the middle of the pattern
                 System.out.print(middle);
                 i++;
            if(length < i && pattern == 2){
                System.out.print(" X "); //prints in the case there are two numbers left over that do not perfectly fit in 3
               }
            }
            i=1;
            System.out.println();
      
           while(length >= i){ //prints the bottom of the pattern 
                   System.out.print(bottom);
                   i++;
                if (length < i && pattern <= 2){
                  System.out.print("/");
                } 
           }    
              
             
             
             
    }
  }