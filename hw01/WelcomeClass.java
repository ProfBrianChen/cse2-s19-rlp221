///////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    /// prints my welcome sign to terminal
    System.out.println("  -----------");  
    
    System.out.println("  | WELCOME | ");

    System.out.println("  -----------");
        
    System.out.println("  ^  ^  ^  ^  ^  ^");
    
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    
    System.out.println("<-R--L--P--2--2--1->");
    
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    
    System.out.println("  v  v  v  v  v  v");
    
    /// print my autobiographical statement to terminal
    System.out.println("Hi! My name is Rebecca Price. I am on the track and field team here at Lehigh and I am an Economics and Finance major.");
  }
  
}
 