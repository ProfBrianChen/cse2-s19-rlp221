///////////////////////////
//////CSE2 BoxVolume
//// Rebecca Price 2/8/19
/// calculates the volume of a box 
/// user provides the length, width, and height used for calculations

import java.util.Scanner;

public class BoxVolume{
      //main method required for every java program
      public static void main(String[] args){
        
        Scanner myScanner = new Scanner( System.in );
        
        //prompts user to enter the width of the box
        System.out.print("Enter the width of the box: ");
        
          double width = myScanner.nextDouble();
        
        //prompts user to enter the length of the box
        System.out.print("Enter the length of the box: ");
          
          double length = myScanner.nextDouble();
        
        //prompts user to enter the height of the box
        System.out.print("Enter the height of the box: ");
        
          double height = myScanner.nextDouble();
        
          //calculated the volume of the box
          double volume = length * width * height;
        
        //states the volume of the box
        System.out.print("The volume insdie the box is " + volume);
        
      }//end of main method
  
}//end of class