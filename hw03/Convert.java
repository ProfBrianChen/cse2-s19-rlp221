////////////////////////////////
//// CSE2 Convert 
//// Rebecca Price 2/8/19
/// Converts meters into inches
/// allows user to insert the amount of meters they would like to convert

import java.util.Scanner;

public class Convert{
            //main method required for every java program
            public static void main (String[] args) {
              
              Scanner myScanner = new Scanner( System.in );
              
              //asks you for the number of meters you wish to measure into inches
              System.out.print("Enter the distance in meters (in the form of xx.xx): ");
              
                double meters = myScanner.nextDouble();
              
                //converts meters into inches
                double inches = meters * 39.37;
              
              //prints the number inches that are in amount of meters you previously stated in myScanner
              System.out.print(meters + " meters is " + inches + " inches.");
              
            }//end of main method
  
}//end of class