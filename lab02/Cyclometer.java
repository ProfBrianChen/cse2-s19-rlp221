////////////////////
//// CSE2 Cyclometer REBECCA PRICE 2/1/19
///
//program used to calculate data given from my bicycle cyclometer to find the number of counts, minutes, and miles the trips took
//measures the time elapsed in seconds and the number of rotations of my front wheel during that time 
public class Cyclometer {
    //main method required for every java program
    public static void main (String[] args){
      
      //our input data
      int secsTrip1=480;  //the number of seconds for the first trip
      
      int secsTrip2=3220;  //the number of seconds for the second trip
      
		  int countsTrip1=1561;  //the number of counts for the first trip
      
		  int countsTrip2=9037; //the number of counts for the second trip
      
      //our immediate variables and output data
      double wheelDiameter=27.0;  //the diameter of the wheel
      
      double PI=3.14159; //the number of Pi, used for calculating the diameter of the wheel
      
  	  int feetPerMile=5280; //the amount of feet within a mile
      
  	  int inchesPerFoot=12; //the amount of inches within a foot
      
  	  double  secondsPerMinute=60; //the amount of seconds in a minute
      
    	double distanceTrip1, distanceTrip2, totalDistance;  //declaring each of these trip distances as doubles 
      
         System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute)+ " minutes and had " + countsTrip1+ " counts.");
      //prints how many minutes and counts trip 1 had
      
	       System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
      //prints how many minutes and counts trip 2 had
             
       distanceTrip1=countsTrip1*wheelDiameter*PI; //gives us the distance in inches
      //for each count, a rotation of the wheel travel the diameter in inches times pi
      
       distanceTrip1/=inchesPerFoot*feetPerMile; //gives distance in miles

       distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives distance in miles
      
       totalDistance=distanceTrip1+distanceTrip2; //total distance of the two trips

       //prints out the data
          System.out.println("Trip 1 was "+distanceTrip1+" miles");
            
	        System.out.println("Trip 2 was "+distanceTrip2+" miles");
      
	        System.out.println("The total distance was "+totalDistance+" miles");

      
    } //end of main method
} //end of class

