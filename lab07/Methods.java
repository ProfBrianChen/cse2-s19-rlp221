///////////////////////////////////////
///////// CSE2 Methods
///// Rebecca Price    3/22/19
///create sentneces by using methods

import java.util.Random;
import java.util.Scanner;

 public class Methods{
   
     
       public static String getAdjectives(){ //Method for adjectives
       
          String Adjectives = "New"; //declares and initializes Adjectives
         
          Random randomGenerator = new Random (); //calls the randomGenerator 
         
          int randomInt = randomGenerator.nextInt(10); //by using randomGenerator randomInt is saved with a number 0 - 9
       
          switch(randomInt) { //based on the random number, the switch will pick a specified adjective
            case 0:
              Adjectives = "happy";
                break;
            case 1:
              Adjectives = "new";
                break;
            case 2:
              Adjectives = "pretty";
                break;
            case 3:
              Adjectives = "old"; 
                break;
            case 4:
              Adjectives = "disgusting";
                break;
            case 5:
              Adjectives = "kind";
                break;
            case 6:
              Adjectives = "blue";
                break;
            case 7:
              Adjectives = "colassal";
                break;
            case 8: 
              Adjectives = "tired";
                break;
            case 9:
              Adjectives = "disgruntled";
                break;
                  
          }
       return Adjectives;
       }

     
       public static String getNonPrimaryNounsSubject(){
         
         String nonPrimaryNounsSubject = "dog";
         
         Random randomGenerator = new Random (); //calls the randomGenerator
         
         int randomInt = randomGenerator.nextInt(10); //by using randomGenerator randomInt is saved with a number 0 - 9
         
         switch(randomInt) { //based on the random number, the switch will pick a specified non primary noung that fits with the dubject
            case 0:
              nonPrimaryNounsSubject = "dog";
                break;
            case 1:
              nonPrimaryNounsSubject = "cat";
                break;
            case 2:
              nonPrimaryNounsSubject = "baby";
                break;
            case 3:
              nonPrimaryNounsSubject = "dad"; 
                break;
            case 4:
              nonPrimaryNounsSubject = "carrot";
                break;
            case 5:
              nonPrimaryNounsSubject = "bunny";
                break;
            case 6:
              nonPrimaryNounsSubject = "toy";
                break;
            case 7:
              nonPrimaryNounsSubject = "kid";
                break;
            case 8: 
              nonPrimaryNounsSubject = "truck";
                break;
            case 9:
              nonPrimaryNounsSubject = "airplane";
                break;
                  
          }
         return nonPrimaryNounsSubject;
       }
     
       public static String getPastTenseVerbs(){
         
         String pastTenseVerbs = "ran";
         
         Random randomGenerator = new Random (); //calls the randomGenerator
         
         int randomInt = randomGenerator.nextInt(10); //by using randomGenerator randomInt is saved with a number 0 - 9
         
          switch(randomInt) { //based on the random number, the switch will pick a specified non primary noung that fits with the dubject
            case 0:
              pastTenseVerbs = "ran";
                break;
            case 1:
              pastTenseVerbs = "sang";
                break;
            case 2:
              pastTenseVerbs = "jumped";
                break;
            case 3:
              pastTenseVerbs = "followed"; 
                break;
            case 4:
              pastTenseVerbs = "hopped";
                break;
            case 5:
              pastTenseVerbs = "destroyed";
                break;
            case 6:
              pastTenseVerbs = "painted";
                break;
            case 7:
              pastTenseVerbs = "threw";
                break;
            case 8: 
              pastTenseVerbs = "drove";
                break;
            case 9:
              pastTenseVerbs = "swam";
                break;
                  
          }
         return pastTenseVerbs;
       }
     
       public static String getNonPrimaryNounsObject(){
         
         String nonPrimaryNounsObject = "cat";
         
         Random randomGenerator = new Random (); //calls the randomGenerator
         
         int randomInt = randomGenerator.nextInt(10); //by using randomGenerator randomInt is saved with a number 0 - 9
         
          switch(randomInt) { //based on the random number, the switch will pick a specified non primary noung that fits with the dubject
            case 0:
              nonPrimaryNounsObject = "dog";
                break;
            case 1:
              nonPrimaryNounsObject = "cat";
                break;
            case 2:
              nonPrimaryNounsObject = "baby";
                break;
            case 3:
              nonPrimaryNounsObject = "dad"; 
                break;
            case 4:
              nonPrimaryNounsObject = "carrot";
                break;
            case 5:
              nonPrimaryNounsObject = "bunny";
                break;
            case 6:
              nonPrimaryNounsObject = "toy";
                break;
            case 7:
              nonPrimaryNounsObject = "kid";
                break;
            case 8: 
              nonPrimaryNounsObject = "truck";
                break;
            case 9:
              nonPrimaryNounsObject = "airplane";
                break;
              
                  
          }
          return nonPrimaryNounsObject;
       }
   
   public static String createSentence(){ //creates the thesis sentence
     
     String Adjectives = getAdjectives();
     String pastTenseVerbs = getPastTenseVerbs();
     String nonPrimaryNounsSubject = getNonPrimaryNounsSubject();
     String nonPrimaryNounsObject = getNonPrimaryNounsObject();
     String Adjectives1 = getAdjectives();
     String Adjectives2 = getAdjectives();
        
        System.out.println("The " + Adjectives + " " + Adjectives1 + " " +  nonPrimaryNounsSubject + " " + pastTenseVerbs + " the " + Adjectives2 + " " + nonPrimaryNounsObject);
        
     return nonPrimaryNounsSubject;
   }
   
   public static String createParagraph(){ //creates the secondary sentences based on the thesis sentence, may not make sense
     
     String subject = createSentence();
     String Adjectives = getAdjectives();
     String Adjectives1 = getAdjectives();
     String pastTenseVerbs = getPastTenseVerbs();
     String pastTenseVerbs1 = getPastTenseVerbs();
     String nonPrimaryNounsObject1 = getNonPrimaryNounsObject();
     String nonPrimaryNounsObject = getNonPrimaryNounsObject();
     String nonPrimaryNounsSubject = getNonPrimaryNounsSubject();
     
     String supportingSentence = "This " + subject + " was " + pastTenseVerbs + " to " + nonPrimaryNounsObject + ". " + "It used " + nonPrimaryNounsObject1 + " to " + pastTenseVerbs1 + " " + nonPrimaryNounsSubject;
     
     return supportingSentence;
   }
   
   public static String getConclusionSentence(){ //creates a conclusion sentence
     
     String subject = createSentence();
     String pastTenseVerbs = getPastTenseVerbs();
     String nonPrimaryNounsObject = getNonPrimaryNounsObject();
     
     String conclusionSentence = "That " + subject + " " + pastTenseVerbs + " her " + nonPrimaryNounsObject;
     
     return conclusionSentence;
      
   }
   
   public static void getParagraph(){
     
     String sentence1 = createSentence();
     
     System.out.print(sentence1);
     
     String sentences2 = createParagraph();
     
     System.out.println(sentences2);
     
     String conclusion = getConclusionSentence();
     
     System.out.println(conclusion);
     
     
     
   }
   
     public static void main (String args[]){
       
       Scanner myScanner = new Scanner (System.in);
       
     boolean check = true;
      while (check == true){ //prints the sentence and calls each variable 
        String Adjectives = getAdjectives();
        String nonPrimaryNounsSubject = getNonPrimaryNounsSubject();
        String pastTenseVerbs = getPastTenseVerbs();
        String nonPrimaryNounsObject = getNonPrimaryNounsObject();
        String Adjectives1 = getAdjectives();
        String Adjectives2 = getAdjectives();
  
        
        System.out.println("The " + Adjectives + " " + Adjectives1 + " " +  nonPrimaryNounsSubject + " " + pastTenseVerbs + " the " + Adjectives2 + " " + nonPrimaryNounsObject);
        
        System.out.println("Would you like another sentence. Please respond with yes or no"); //
        
        String junkWord = "";
        
        while(myScanner.hasNext() == false){
          
          junkWord = myScanner.next();
          
          System.out.println("Please respond with yes or no");
          
        }
        
        String userResponse = myScanner.next();
        
        Adjectives = getAdjectives();
        nonPrimaryNounsSubject = getNonPrimaryNounsSubject();
        pastTenseVerbs = getPastTenseVerbs();
        nonPrimaryNounsObject = getNonPrimaryNounsObject();
        Adjectives1 = getAdjectives();
        Adjectives2 = getAdjectives();
          
          if (userResponse.equals("yes") || userResponse.equals("YES") || userResponse.equals("Yes")){ //if yes then create a new sentence
            System.out.println("The " + Adjectives + " " + nonPrimaryNounsSubject + " " + pastTenseVerbs + " " + Adjectives1 + " " + nonPrimaryNounsObject);
              break;
          }
        
          else{
            break;
          }
      }
       
     getParagraph();
   }
 }