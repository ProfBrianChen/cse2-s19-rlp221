//////////////////////////////////
/// CSE2    lab10
/// Rebecca Price   4/19/19
/// print out and operate on row major and column major matrices

public class lab10{
   
   public static void main(String args[]){
      int widthOfArray = (int)(Math.random()*5 + 1); //random width
      int heightOfArray = (int)(Math.random()*5 + 1); //random height
      int[][] A = increasingMatrix(widthOfArray, heightOfArray, true); //initializes values of the array
      System.out.println("Array A is:");
      printMatrix(A, widthOfArray, heightOfArray, true); //prints values of the array
      int[][] B = increasingMatrix(widthOfArray, heightOfArray, false); //initializes values of array B
      System.out.println("Array B is:");
      printMatrix(B, widthOfArray, heightOfArray, false); //prints matrix of array B
      int widthOfArray1 = (int)(Math.random()*5 + 1);
      int heightOfArray1 = (int)(Math.random()*5 + 1);
      int[][] C = increasingMatrix(widthOfArray1, heightOfArray1, true); //is matrix C supposed to be true or false!!!!!
      System.out.println("Array C is:");
      printMatrix(C, widthOfArray1, heightOfArray1, true); //print array c, not sure if it is meant to be in row major or column major
      addMatrix(A, true, B, false);
      
      
   }
   
   public static int[][] increasingMatrix(int width, int height, boolean format){  //format is true if the matrix is in row major format
      int[][] Array = new int[height][width];
      if(format == true){ //create a matrix in row major format
         int[][] myArray1 = new int[height][width];
         int num = 0; //set a number so that each value can be increased by 1 within the loop
         for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
               myArray1[i][j] = num + 1;
               num = num + 1;
            }
         }
         return myArray1;
      }
      
     if(format == false){ //create a matrix in column major format
   
         int[][] myArray2 = new int[width][height];
         for(int i = 0; i < width; i++){
            int num = 1 + i; //resets the number after each 2 dimensional array 
            for(int j = 0; j < height; j++){
               myArray2[i][j] = num; //ensures the number saved is added by the number of the width because it increases by 1 along the row 
               num = num + width;
            }
         }
         return myArray2;
      }
     return Array; //incase both situations do not hold true an array with the memory of the height and width will be returned without initialized values
   }
   
   public static void printMatrix(int[][] Array, int width, int height, boolean format){
      if(format == true){ //prints the arrays if it were a row major
         for(int i = 0; i < height; i++){ 
            for(int j = 0; j < width; j++){
            System.out.print(Array[i][j] + " ");
            }
            System.out.println();
         }
         
      }
      if(format == false){ //prints the arrays if it were column major
         for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
               System.out.print(Array[j][i] + " ");
            }
            System.out.println();
         }
      }
      
      if(Array == null){ //print that the array is empty if the array has no values
         System.out.println("The array was empty!");
      }
   }
   
   public static int[][] translate(int[][] Array){ //FIX!
      int height = Array.length;
      int width = Array[0].length;
      int[][] rowArray = new int[width][height];
      //System.out.println("the new array is:");
         for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
               rowArray[i][j] = Array[j][i];
               //System.out.print(rowArray[i][j]);
            }
            //System.out.println();
         }
      return rowArray;
      }
      
   public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
      int heighta = a.length;
      int heightb = b.length;
      int widtha = a[0].length;
      int widthb = b[0].length;
      
      if(widtha == widthb && heighta == heightb){//check to see if the two are able to be added
      
         if(formata == false){//use translate array to translate a column major to a row major so that they can be added
             a = translate(a);
         }
         if(formatb == false){
             b = translate(b);
         }
         int[][] addArray = new int [heighta][widtha];
         System.out.println("The added array is:");
         for(int i = 0; i < heighta; i++){
            for(int j = 0; j < widtha; j++){
               addArray[i][j] = a[i][j] + b[i][j];
               System.out.print(addArray[i][j] + " ");
            }
            System.out.println();
         }
         return addArray;
      }
      else{
         System.out.println("The arrays cannot be added!");
       return null;
      }
   }
   









}