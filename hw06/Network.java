//////////////////////////////////////
///// CSE2 Network
/// Rebecca Price 3/11/19
// program meant to make a series of windows depending on users inputs for height, width, size, and length

import java.util.Scanner;
  public class Network{
    public static void main (String args[]){
      
      Scanner myScanner = new Scanner(System.in);
      
      String junkWord; 
      
      System.out.println("input your desired width");
      
   while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer
      
      System.out.println("Error: please type in an integer");
      
      junkWord = myScanner.next();

    }
      int width = myScanner.nextInt(); //users input saved in width
      
   while (width < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Error: not a positive integer");
      
      width = myScanner.nextInt();
        
      }
      
    
      System.out.println("input your desired height");
      
   while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer
      
      System.out.println("Error: please type in an integer");
      
      junkWord = myScanner.next();

    }
      int height = myScanner.nextInt(); //users input saved in height
    
   while (height < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Error: not a positive integer");
      
      height = myScanner.nextInt();
        
      }
    
   System.out.println("input your desired size");
      
   while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer
      
      System.out.println("Error: please type in an integer");
      
      junkWord = myScanner.next();

    }
      int size = myScanner.nextInt(); //users input saved in height
    
   while (size < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Error: not a positive integer");
      
      size = myScanner.nextInt();
        
      }
      
    System.out.println("input your desired length");
      
   while(myScanner.hasNextInt() == false){ //checks to make sure the user entered an integer and if not prompts them to enter an integer
      
      System.out.println("Error: please type in an integer");
      
      junkWord = myScanner.next();

    }
      int length = myScanner.nextInt(); //users input saved in height
    
   while (length < 1){//ensures that the input was within the required range of 1-10
        
      System.out.println("Error: not a positive integer");
      
      length = myScanner.nextInt();
        
      }
      
      //code for making the shapes
      
     int oddOrEven = size % 2; 
     int leftOvers = width % (size + length);
     int heightLeftOvers = height % (size + length);
     int widthRepetitions = (width - leftOvers) / (length + size); 
     int heightRepetitions = (height - heightLeftOvers) / (size + length);
     int halfway = (size - 2) / 2;
      
     
       if(oddOrEven == 1){                   
          for(int j = 0; j < heightRepetitions; j++){ //repeats the top and bottom of each square
            for(int a = 0; a < 2; a++){
              int i = 0;
              if (i + length <= width){
              while (i + length < width){
                 System.out.print("#");
                 i++;
                for(int l = 0; l < size - 2; l++){ //prints the dash repeatedly until l is no longer less than size - 2
                  System.out.print("-");
                  i++;
                }
                if(size == 1){
                   for(int l = 0; l < length; l++){
                     System.out.print("-");
                     i++;
                   }
                }
                if(size != 1){
                  System.out.print("#");
                  i++;
                  for(int l = 0; l < length; l++){ //prints spaces between each pattern for the first and last lines
                    System.out.print(" ");
                    i++;
                  }
                }
              }  
            }
             else if (i + length > width){ // prints the leftover of the pattern if it doesnt perfectly fit into width
                 System.out.print("#");
                 i++;
                while (i < width){ //print dash until it is less than or equal to 
                  System.out.print("-");
                  i++;
              }  
             }
               //increase height so it does not over repeat or under repeat at the end 
              System.out.println();
          if (a < 1){   
            for(int p = 0; p < size - 2; p++){
                i = 0;
                while(i< width){ //prints the pattern as many time as the width pattern can be fully repeated
                    for(int m = 0; m < 2; m++){ //prints it repeatedly on lines
                        System.out.print("|");
                        i++; 
                      if (m < 1){
                        for(int n = 0; n < size - 2; n++){ //prints a space repeatedly within a row
                             System.out.print(" "); 
                              i++;
                        }
                      }
                    }
                  if(i + length < width){
                    for(int l = 0; l < length; l++){ //prints spaces between each repition of the pattern based on how long length is
                     // if(leftOvers < )
                      if(p == halfway){ //ensures the length dashes are only printed in the middle of the pattern 
                         System.out.print("-");
                          i++;
                        } 
                        else{
                         System.out.print(" "); 
                         i++;
                        } 
                      }
                    }
                  else{
                    while(i < width){
                      if(p == halfway){ //ensures the length dashes are only printed in the middle of the pattern 
                         System.out.print("-");
                          i++;
                        } 
                        else{
                         System.out.print(" "); 
                         i++;
                        } 
                    }
                  }
                  }
              if(p < size - 3){
                System.out.println(); //prints to next line, need to make sure it doesnt print an extra line before completing the square
               } 
               //how many times a line repeats
            }
          }
            if (a >= 1){
             if(size != 1){
              for (int l = 0; l < length; l++){
                for(i = 0; i < width; i++){
                  for (int m = 0; m < 2; m++){ //repeats the line after each box, but ensures that the line with only print once while the spaces print twice
                    for(int p = 0; p < size/2; p++){
                      System.out.print(" ");
                      i++;
                    }
                    if(m < 1){
                      System.out.print("|");
                      i++;
                    }
                  }
                  for(int s = 0; s < length; s++){ //prints the spaces between each box
                    System.out.print(" ");
                    i++;
                  }
                }
                if (l < length - 1){ //doesnt print an extra line unless needed
                  System.out.println();
                }
               }
             }
            }
              
           if (size == 1){
             for (int l = 0; l < length; l++){
               for(i = 0; i < width; i++){
                 System.out.print("|");
                 i++;
                 for(int s = 0; s < length; s++){
                   System.out.print(" ");
                   i++;
                 }
               }
               System.out.println();
             }
           }
              if (size != 1){
                System.out.println();
              }
            
              if(size == 1){
                a++;
              }
          }
        }
      }
      
      
      if(oddOrEven == 0){                   
          for(int j = 0; j < heightRepetitions; j++){ //repeats the top and bottom of each square
            for(int a = 0; a < 2; a++){
              for (int i = 0; i < width; i++){
                 System.out.print("#");
                 i++;
                for(int l = 0; l < size - 2; l++){ //prints the dash repeatedly until l is no longer less than size - 2
                  System.out.print("-");
                  i++;
                }
                if(size == 1){
                   for(int l = 0; l < length; l++){
                     System.out.print("-");
                     i++;
                   }
                }
                if(size != 1){
                  System.out.print("#");
                  i++;
                  for(int l = 0; l < length; l++){ //prints spaces between each pattern for the first and last lines
                    System.out.print(" ");
                    i++;
                  }
                }
              }   
              System.out.println();
          if (a < 1){   
            for(int p = 0; p < size - 2; p++){
              int i = 0;
                while(i < width){ //prints the pattern as many time as the width pattern can be fully repeated
                    for(int m = 0; m < 2; m++){ //prints it repeatedly on lines
                        System.out.print("|");
                        i++;
                      if (m < 1){
                        for(int n = 0; n < size - 2; n++){ //prints a space repeatedly within a row
                             System.out.print(" "); 
                             i++;
                        }
                      }
                    }
                  if(i + length < width){
                    for(int l = 0; l < length; l++){ //prints spaces between each repition of the pattern based on how long length is
                      if(p == halfway || p == halfway - 1){ //ensures the length dashes are only printed in the middle of the pattern 
                         System.out.print("-");
                         i++;
                       } 
                      else{
                       System.out.print(" "); 
                       i++;
                     } 
                   }
                  } 
                 if(i + length > width){
                   while (i < width){
                     if(p == halfway || p == halfway - 1){ //ensures the length dashes are only printed in the middle of the pattern 
                         System.out.print("-");
                         i++;
                       } 
                      else{
                       System.out.print(" "); 
                       i++;
                     } 
                   }
                 }
                }
              if(p < size - 3){
                System.out.println(); //prints to next line, need to make sure it doesnt print an extra line before completing the square
               }
            }
          }
            if (a >= 1){
              for (int l = 0; l < length; l++){
                for(int i = 0; i < width; i++){
                  for (int m = 0; m < 2; m++){ //repeats the line after each box, but ensures that the line with only print once while the spaces print twice
                    for(int p = 0; p < size/2 - 1; p++){
                      System.out.print(" ");
                      i++;
                    }
                    if(m < 1){
                      System.out.print("||");
                      i++;
                      i++;
                    }
                  }
                  for(int s = 0; s < length; s++){ //prints the spaces between each box
                    System.out.print(" ");
                    i++;
                  }
                }
                if (l < length - 1){ //doesnt print an extra line unless needed
                  System.out.println();
                }
              }
            }
              System.out.println();
          }
        }
      }
      
      //im sorry to whomever has to grade this
      // i tried my best but i give up
    }
  }
    
    
    